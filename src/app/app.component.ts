  import { Component, OnInit } from '@angular/core';
  import { NumberService } from './services/number.service';
  import { FormBuilder, Validators } from '@angular/forms';
  import { UserInput } from './models/user_input.model';

  @Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
  })
  export class AppComponent implements OnInit{

    //This will hide and show results heading
    showResults = false;

    heading = '';

    numbersForm = this.fb.group({
      startIndex: [0, Validators.required],
      endIndex: [0, Validators.required],
    });

    //Results from our Api
    results = [];

    userInput: UserInput = new UserInput();

    constructor(private numberService: NumberService,private fb: FormBuilder)
  {
    this.heading = 'Nothing to show';
  }

    ngOnInit(): void {
    }

    onSubmit() {

      if(this.numbersForm.valid){
        this.userInput.startIndex = this.numbersForm.value['startIndex'];
        this.userInput.endIndex = this.numbersForm.value['endIndex'];
      }

      this.numberService.ShowMultiples(this.userInput).subscribe(
        response => {
          if(response.success){
            this.onSuccess(response);
          }else{
            this.onError(response);
          }
          console.info(response);
        },
        error => {
          this.heading = 'Error - Something went wrong';
          console.error(error);
        })
    }

    onSuccess(response){
    //This can also come api, passed through a message
    this.heading = 'Showing results for '+ this.userInput.startIndex + ' - ' + this.userInput.endIndex;
    this.results = response.data;
    this.showResults = true;
    this.userInput.startIndex = 0;
    this.userInput.endIndex = 0;
    }

    onError(response){
            this.userInput.startIndex = 0;
            this.userInput.endIndex = 0;
            this.results = [];
            //Show error message from api
            this.heading = response.message;
    }
  }
    