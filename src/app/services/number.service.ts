import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NumberService {

  results: any;
  headersConfig;

  constructor(private http: HttpClient) {
  }

  ShowMultiples(post) {
    return this.http.post<any>(environment.API_URL + '/api/ShowMultiples', post, { headers: this.httpHeader() })
      .pipe(
        catchError(e => throwError(e))
      );
  }

  httpHeader(){
    let headerJson = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    };
   return new HttpHeaders(headerJson);
  }

}
